## Enable content Copy/Paste between VMRC client and Windows/Linux Virtual Machine (57122)
> Fuente: https://kb.vmware.com/s/article/57122

From the vCenter Server HTML5 Web Client
1. Power off the VM.
2. Right-click the virtual machine and click Edit Settings.
- Click the VM Options tab, expand Advanced, and click Edit Configuration.
- Click on Add Configuration Params three times to give three rows 
- Fill in the Name and Value fields as mentioned below:

| Name | Value |
| ------ | ------ |
| isolation.tools.copy.disable | FALSE |
| isolation.tools.paste.disable | FALSE |
| isolation.tools.setGUIOptions.enable | TRUE |

- Click OK to save and exit out of the Configuration Parameters wizard. Note: These options override any settings made in the guest operating system’s VMware Tools control panel. 
- Click OK to save and exit out of the Edit Settings wizard.
3. Power on the VM
4. Then use Copy/Paste directly on Windows/Linux/any other platform. 
